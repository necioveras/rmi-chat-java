
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public interface Servidor extends Remote {
  public  void adicionaMensagem(String mensagem) throws RemoteException;
  public  void registraCliente(Cliente c, String nome) throws RemoteException;
}