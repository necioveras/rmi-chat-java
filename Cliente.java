
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public interface Cliente extends Remote {
   public void atualizaClient(String q) throws RemoteException;
   public boolean checkClient() throws RemoteException;
   public String verNome() throws RemoteException;
}