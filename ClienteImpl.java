
import java.awt.*;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.Naming;
import java.awt.event.*;
import java.io.Serializable;

import javax.swing.JOptionPane;


class ClienteImpl extends Frame implements Cliente, Serializable {
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
BorderLayout borderLayout1 = new BorderLayout();
  TextArea textArea1 = new TextArea();
  Panel panel1 = new Panel();
  Label label1 = new Label();
  Button button1 = new Button();
  TextField textField1 = new TextField();
  
  private String nome; 

  private Servidor servidor = null;

  public void atualizaClient(String q) throws RemoteException {
    textArea1.append(q+"\n");
  }

  public ClienteImpl() {
	nome = JOptionPane.showInputDialog("Entre com seu nome para participar do chat:");
    try {
      exportaObjeto();
      jbInit();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  }
  private void jbInit() throws Exception {	  		  
    this.setLayout(borderLayout1);
    label1.setFont(new java.awt.Font("Dialog", 1, 11));
    label1.setText("Mensagem:");
    button1.setLabel("Enviar:");
    button1.addActionListener(new ClienteImpl_button1_actionAdapter(this));
    textField1.setText("Digite aqui sua mensagem...");
    textArea1.setText("");
    this.addWindowListener(new ClienteImpl_this_windowAdapter(this));
    this.add(textArea1, BorderLayout.CENTER);
    this.add(panel1, BorderLayout.NORTH);
    panel1.add(label1, null);
    panel1.add(textField1, null);
    panel1.add(button1, null);        
    servidor.adicionaMensagem(nome + " entrou no chat");
  }

  private void exportaObjeto(){
    try {
      // Exportar o objeto
      UnicastRemoteObject.exportObject(this,0);
    }
    catch (RemoteException e) {
      System.out.println("Nao consegui exportar o objeto");
      System.out.println(e);
      System.exit(0);
    }
    try {
      // Obter a referencia do Servidor
      servidor = (Servidor) Naming.lookup("rmi://localhost:8099/Servidor");
      servidor.registraCliente(this, nome);
    }
    catch (RemoteException e) {
      System.out.println("Nao consegui registrar o cliente no servidor");
      System.out.println(e.toString());
    }
    catch (MalformedURLException mal){
    	System.out.println(mal.toString());
    }
    catch (NotBoundException not){
    	System.out.println(not.toString());
    }
  }

  public static void main(String[] args) {
    Frame f = new ClienteImpl();
    f.setSize(400,300);
    f.setVisible(true);
  }

  void button1_actionPerformed(ActionEvent e) {
    try {
        System.out.println(""+ textField1.getText());
        servidor.adicionaMensagem((String) nome + " diz: " + textField1.getText());
    } catch (Exception ex) {
        System.out.println(ex.toString());
    }
  }

  void this_windowClosing(WindowEvent e) {
    System.exit(0);
  }

@Override
public boolean checkClient() throws RemoteException {
	return true;  //"estou vivo"
}

@Override
public String verNome() throws RemoteException {
	return nome;
}

public String getNome() {
	return nome;
}

}

class ClienteImpl_button1_actionAdapter implements java.awt.event.ActionListener {
  ClienteImpl adaptee;

  ClienteImpl_button1_actionAdapter(ClienteImpl adaptee) {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e) {
    adaptee.button1_actionPerformed(e);
  }
}

class ClienteImpl_this_windowAdapter extends java.awt.event.WindowAdapter {
  ClienteImpl adaptee;

  ClienteImpl_this_windowAdapter(ClienteImpl adaptee) {
    this.adaptee = adaptee;
  }
  public void windowClosing(WindowEvent e) {
    adaptee.this_windowClosing(e);
  }
}