
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/**
 * <p>
 * Title:
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2003
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */

public class ServidorImpl implements Runnable, Servidor {

	private Map<Cliente, String> listaClientes; // tracks all connected clients
	private Vector<String> listaMensagens; // maintains updatable list of quotes
	private Thread clienteThread = null;
	boolean flag_stop = false;

	public ServidorImpl() {
		listaMensagens = new Vector<>();
		//listaMensagens.addElement("Esta mensagem eh um teste");
		//listaMensagens.addElement("Esta mensagem eh tambem eh teste");
		//listaClientes = new Vector();
		listaClientes = new HashMap<>();
		
	}

	public void run() {
		while (!flag_stop) {
			try {
				Thread.currentThread();
				// sleep for a second
				Thread.sleep(1000);
			} catch (Exception e) {
			}
			verificaCliente();
		}
		flag_stop = false;
	}

	private void verificaCliente() {
		synchronized (listaClientes) {			
			try{
				for (Cliente c : listaClientes.keySet()) {
					String nome_temp = (String) listaClientes.get(c);
					try {
						// verifica o cliente, via callback
						c.checkClient();
					} catch (RemoteException e) {
						// retira a referencia do cliente que foi desconectado
						listaClientes.remove(c);	
						System.out.println("cliente deve ter sido desconectado !");
						try{
							adicionaMensagem(nome_temp + " saiu do chat ");
						} catch (RemoteException r){}
										
						if (listaClientes.keySet().size() <= 0) {
							// no more clients- so stop server thread						
							this.flag_stop = true;
							// Thread dummy = clientThread;
							// clientThread = null;
							// dummy.stop();
						}
					}				
				}
			} catch (Exception e){}
				
			//listaClientes.clear();
			//listaClientes.putAll(bkp);
		} // end syncronization on clientele
	}

	public void adicionaMensagem(String mensagem) throws RemoteException {
		synchronized (listaMensagens) {
			listaMensagens.addElement(mensagem);
			//notifia todos os clientes conectados
			for (Cliente c : listaClientes.keySet())				
				c.atualizaClient(mensagem);
		}
	}

	public void registraCliente(Cliente c, String nome) throws RemoteException {
		synchronized (listaClientes) {
			listaClientes.put(c, nome);
		}
		if (clienteThread == null) {
			clienteThread = new Thread(this, "clienteThread");
			clienteThread.start();
		}
	}

	public static void main(String[] args) {
//		System.setSecurityManager(new RMISecurityManager());
		try {
			System.out.println("Criando registry...");
			Registry reg = null;
			try {
				reg = LocateRegistry.createRegistry(8099);
			} catch (Exception ex) {
				try {
					reg = LocateRegistry.getRegistry(8099);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			System.out.println("Criando servidor...");
			ServidorImpl servidor = new ServidorImpl();
			Servidor stub_servidor = (Servidor) UnicastRemoteObject.exportObject(servidor,0);
			System.out.println("Ligando servidor ao Registry...");
			// Naming.rebind("rmi://localhost:8099/Servidor", servidor);
			reg.rebind("Servidor", stub_servidor);
			System.out.println("Servidor ligado ao Registry");
		} catch (Exception e) {
			System.out.println("Problema ao conectar servidor no Regitry (binding)");
			System.out.println(e.toString());
		}
	}

}